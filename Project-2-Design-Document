             +--------------------------+
             |          CS 140          |
             | PROJECT 2: USER PROGRAMS |
             |     DESIGN DOCUMENT      |
             +--------------------------+

---- GROUP ----

>> Fill in the names and email addresses of your group members.

Marc Ivan Manalac <marcivan@hawaii.edu>
Alex Bozyck <abozyck@hawaii.edu>
Mark Pascua <email>
Joshua Subia <jsubia22@hawaii.edu>

---- PRELIMINARIES ----

>> If you have any preliminary comments on your submission, notes for the
>> TAs, or extra credit, please give them here.

>> Please cite any offline or online sources you consulted while
>> preparing your submission, other than the Pintos documentation, course
>> text, lecture notes, and course staff.

               ARGUMENT PASSING
               ================

---- DATA STRUCTURES ----

>> A1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.
1) struct intrupt_frame if_ : Used in setting up Stack interupt frame
2) struct list_elemnts *e : 
3) struct child *ch=NULL : Child initialization in process_wait
4) struct list_elemen *e1=NULL : list initializations in process_wait
5) struct child *f : List of Child processes, sets up process_wait
6) struct thread *t = thread_current () : Initializing current thread, used frequently


---- ALGORITHMS ----

>> A2: Briefly describe how you implemented argument parsing.  How do
>> you arrange for the elements of argv[] to be in the right order?
>> How do you avoid overflowing the stack page?

To parse the arguments, we used the strtok_r() function within the 
setup_stack() portion of process.c. Each argument is saved into the save 
pointer as well as counted into argc then we initialize the pointer argv 
using calloc. We loop through each argument, then decrementing the stack 
pointer, copy the size of the stack pointer, then set that argv index to 
that stack pointer.

---- RATIONALE ----

>> A3: Why does Pintos implement strtok_r() but not strtok()?

The function strtok_r() is the reentrant version of strtok(). That is, 
strtok_r() can be interrupted and called again before finishing the process. 
That is because of the extra parameter, saveptr which is a static pointer. 
Pintos requires us to separate our arguments and store them elsewhere, 
waiting till they are needed.

>> A4: In Pintos, the kernel separates commands into a executable name
>> and arguments.  In Unix-like systems, the shell does this
>> separation.  Identify at least two advantages of the Unix approach.

1) Decreases Complexity of Security by inheriting privileges of parent programs

2) Flexibility increases, each user using a different shell. The kernel need not change each time.

                 SYSTEM CALLS
                 ============

---- DATA STRUCTURES ----

>> B1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.
struct thread
  {
    /* Owned by thread.c. */
    tid_t tid;                      // Thread identifier to identify threads. //
    enum thread_status status;      // Thread state and status. //
    char name[16];                  // Name  //
    uint8_t *stack;                 // Saved stack pointer. //
    int p;                          // Priority of Threads. //
    struct list_elem all;           // List of all threads


struct child {
      int tid;                      //Thread identifier
      struct list_elem elem;        //List of all threads
      int exit_error;               //Error when thread trying to use is already in use
      bool used;                    //Thread used or not

    struct list child_proc;         //Listing of the child processes
    struct thread* parent;          //Parent thread
    struct file *itself;            //Pointer to the file itself
    struct list files;              //List of files

    struct semaphore child_lock;    //Semaphore Lock for the child processes
   
>> B2: Describe how file descriptors are associated with open files.
>> Are file descriptors unique within the entire OS or just within a
>> single process?

File Descriptors in this case have a mapping of one to one through the system calls. Allowing for the file descriptor within the Operating System to be exclusive. The reason we implement a list struct is to not put too much information on the thread struct, as warned about from the first project.

---- ALGORITHMS ----

>> B3: Describe your code for reading and writing user data from the
>> kernel.

Reading and writing user data from the kernel is done using the system
calls SYS_WRITE and SYS_READ. 
For SYS_WRITE, we first check if the fd 
(file descriptor) is true, and if it is, pass the buffer and length into
putbuf. Else, a proc_file* fptr  is searched for. If fptr is NULL, f->eax
is set to -1. Else, f->eax is equal to the function file_write_at with 
arguments of fptr->ptr, buffer, length, and zero.
For SYS_READ, the process is almost identical to SYS_WRITE, except that if 
the file descriptor exists, then f->eax is equal to the function file_read_at
with the inputs of fptr->ptr, buffer, length, and zero.

>> B4: Suppose a system call causes a full page (4,096 bytes) of data
>> to be copied from user space into the kernel.  What is the least
>> and the greatest possible number of inspections of the page table
>> (e.g. calls to pagedir_get_page()) that might result?  What about
>> for a system call that only copies 2 bytes of data?  Is there room
>> for improvement in these numbers, and how much?
The greatest number of inspections for a full page is 4,096 bytes if
each segment can only contains one byte. The least number of 
inspections for 4,096 bytes is one, due to the entire 4,096 byte segment
located within one page.
An improvement to this would be to make segments contain the same amount
of bytes that a page can contain, and make a page contain more than one segment.

>> B5: Briefly describe your implementation of the "wait" system call
>> and how it interacts with process termination.

>> B6: Any access to user program memory at a user-specified address
>> can fail due to a bad pointer value.  Such accesses must cause the
>> process to be terminated.  System calls are fraught with such
>> accesses, e.g. a "write" system call requires reading the system
>> call number from the user stack, then each of the call's three
>> arguments, then an arbitrary amount of user memory, and any of
>> these can fail at any point.  This poses a design and
>> error-handling problem: how do you best avoid obscuring the primary
>> function of code in a morass of error-handling?  Furthermore, when
>> an error is detected, how do you ensure that all temporarily
>> allocated resources (locks, buffers, etc.) are freed?  In a few
>> paragraphs, describe the strategy or strategies you adopted for
>> managing these issues.  Give an example.

---- SYNCHRONIZATION ----

>> B7: The "exec" system call returns -1 if loading the new executable
>> fails, so it cannot return before the new executable has completed
>> loading.  How does your code ensure this?  How is the load
>> success/failure status passed back to the thread that calls "exec"?

The design is recording the childs load status and store into its respectable parent threads. The childs job is to set this load status by obtaining the parent thread through the parent id. A helper function in getting the parent thread is thread_getid.

Neededing to allow the child thread to exit at any point in time is a big reason we chose this design. If the thread exits without the parent monotoring it, there will be no way to recapture the status of the thread. 

This is where the checker comes into play. The checker is activated when a child loads, whether it be success or failure. Child obtains the parent id , sets value inside the parent, and also signals the parent. Upon creation of a child thread, the parent sets child load to 0. Parent waits for the child load to != 0. 

>> B8: Consider parent process P with child process C.  How do you
>> ensure proper synchronization and avoid race conditions when P
>> calls wait(C) before C exits?  After C exits?  How do you ensure
>> that all resources are freed in each case?  How about when P
>> terminates without waiting, before C exits?  After C exits?  Are
>> there any special cases?

The Child status struct serves each process of the child. The list inside the parent's struct serves all the child processes. Checker here is preventing a race condition.

Necessary for the child to set up status in the parents thread struct because when the parent leaves, the list inside will then be free, allowing the child to continue.

Before C exits, P calls wait(C). The checker checks P and waits for the checker to check the child with thread_get_the_id. This function checks the entire thread list. 

When P calls wait(C) after the existence of C, P obtains the checker, thereby finding out C already exists. 

If P terminates without waiting for C to exist, the lock releases, making the list free, allowing no signal. C will therefore ignore the checker and continue to execute.  

---- RATIONALE ----

>> B9: Why did you choose to implement access to user memory from the
>> kernel in the way that you did?

The Child status struct serves each process of the child. The list inside the parent's struct serves all the child processes. Checker here is preventing a race condition.



>> B10: What advantages or disadvantages can you see to your design
>> for file descriptors?
Two advantages are:
1)Struct in thread space is minimized
2)Allowing more flexibility to use opened files through the kernel being aware of all the open files

>> B11: The default tid_t to pid_t mapping is the identity mapping.
>> If you changed it, what advantages are there to your approach?

Did not change it, it is reasonable already.

               SURVEY QUESTIONS
               ================

Answering these questions is optional, but it will help us improve the
course in future quarters.  Feel free to tell us anything you
want--these questions are just to spur your thoughts.  You may also
choose to respond anonymously in the course evaluations at the end of
the quarter.

>> In your opinion, was this assignment, or any one of the three problems
>> in it, too easy or too hard?  Did it take too long or too little time?

This assignment was very difficult for our group. We did not feel prepared to tackle all of these problems with the given resources. I think we needed more guidance. It took a large amount of time just figuring out how to setup the stack.

>> Did you find that working on a particular part of the assignment gave
>> you greater insight into some aspect of OS design?

This has definitely given us more insight on the scope of OS design when it comes to managing the stack and how all that is entailed in user program interfacing.

>> Is there some particular fact or hint we should give students in
>> future quarters to help them solve the problems?  Conversely, did you
>> find any of our guidance to be misleading?

We found the guidance given in class to be a bit vague. It would be ideal to have a more personalized guide on how to tackle each problem based on the work we have done in class, pointing to specific topics in chapters and assignments that we have covered.

>> Do you have any suggestions for the TAs to more effectively assist
>> students, either for future quarters or the remaining projects?

We did not have a TA.

>> Any other comments?

It would be nice to spend some time in class to talk about where each group was in the projet and how they solved different problems so we could see which solutions would be more efficient and how far off we were from completing a specific task.
