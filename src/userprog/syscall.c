#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "process.h"//EDIT. Added process.h
#include "threads/vaddr.h"//EDIT. added vaddr.h 
extern bool running;

static void syscall_handler (struct intr_frame *);

/*EDIT. Added helper functions*/
struct proc_file* list_search(struct list* files, int fd);
void *check_addr(const void*);

//Added struct
struct proc_file {
	struct file* ptr;
	int fd;
	struct list_elem elem;
};
//END EDIT


void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f UNUSED) 
{
  //EDIT. Creating pointer variables.
  int *ptr = f->esp;//pointer for the  struct intr_frame on top page pointing to the stack pointer
  int sys_call = *ptr;//
  //END EDIT

  //printf ("system call!\n");

  //EDIT. Conditionals 
  if(sys_call == SYS_WRITE){
    check_addr(*(ptr+6));//
    if(*(ptr+5)==1){
      //if fd is true, pass putbuf the buffer and length
      putbuf(*(ptr+6),*(ptr+7));
      f->eax=*(ptr+7);//
    }
    else{
        struct proc_file* fptr = list_search(&thread_current()->files, *(ptr+5));
	if(fptr==NULL)
   	  f->eax=-1;
	else
	  f->eax = file_write_at (fptr->ptr, *(ptr+6), *(ptr+7),0);
    }
  }
  else if(sys_call == SYS_EXIT){
    thread_current()->parent->exec = true;
    thread_current()->exit_error= *(ptr+1);
    thread_exit();
  }
  else if(sys_call == SYS_EXEC){
    hex_dump(*(ptr+1),*(ptr+1),64,true);
    check_addr(*(ptr+1));
    f->eax = process_execute(*(ptr+1));
  }
  else if(sys_call == SYS_CREATE){
    check_addr(*(ptr+4));
    f->eax = filesys_create(*(ptr+4),*(ptr+5));
  }
  else if(sys_call == SYS_REMOVE){
		if(filesys_remove(*(ptr+1))==NULL)
			f->eax = false;
		else
			f->eax = true;
  }
  else if (sys_call == SYS_OPEN){
	check_addr(*(ptr+1));
	struct file* fptr = filesys_open (*(ptr+1));
	if(fptr==NULL)
		f->eax = -1;
	else{
		struct proc_file *pfile = malloc(sizeof(*pfile));
		pfile->ptr = fptr;
		pfile->fd = thread_current()->fd_count;
		thread_current()->fd_count++;
		list_push_back (&thread_current()->files, &pfile->elem);
		f->eax = pfile->fd;
	}
  }
  else if( sys_call == SYS_FILESIZE){
    f->eax = file_length (list_search(&thread_current()->files, *(ptr+1))->ptr);

 }
 else if( sys_call == SYS_READ){
    check_addr(*(ptr+6));
		if(*(ptr+5)==0)
		{
			int i;
			uint8_t* buffer = *(ptr+6);
			for(i=0;i<*(ptr+7);i++)
				buffer[i] = input_getc();
			f->eax = *(ptr+7);
		}
		else
		{
			struct proc_file* fptr = list_search(&thread_current()->files, *(ptr+5));
			if(fptr==NULL)
				f->eax=-1;
			else
				f->eax = file_read_at (fptr->ptr, *(ptr+6), *(ptr+7),0);
		}
 }
  else{
    printf("No sys call implemented for that yet\n");
  }
  //thread_exit ();
  //END EDIT.
}

void* check_addr(const void *vaddr)
{
	if (!is_user_vaddr(vaddr))
	{
		thread_current()->parent->exec = true;
		thread_current()->exit_error = -1;
		thread_exit();
		return 0;
	}
	void *ptr = pagedir_get_page(thread_current()->pagedir, vaddr);
	if (!ptr)
	{
		thread_current()->parent->exec = true;
		thread_current()->exit_error = -1;
		thread_exit();
		return 0;
	}
	return ptr;
}

struct proc_file* list_search(struct list* files, int fd)
{

	struct list_elem *e;

      for (e = list_begin (files); e != list_end (files);
           e = list_next (e))
        {
          struct proc_file *f = list_entry (e, struct proc_file, elem);
          if(f->fd == fd)
          	return f;
        }
   return NULL;
}

void close_file(struct list* files, int fd)
{

	struct list_elem *e;

      for (e = list_begin (files); e != list_end (files);
           e = list_next (e))
        {
          struct proc_file *f = list_entry (e, struct proc_file, elem);
          if(f->fd == fd)
          {
          	file_close(f->ptr);
          	list_remove(e);
          }
        }
}

void close_all_files(struct list* files)
{

	struct list_elem *e;

      for (e = list_begin (files); e != list_end (files);
           e = list_next (e))
        {
          struct proc_file *f = list_entry (e, struct proc_file, elem);

	      	file_close(f->ptr);
	      	list_remove(e);
        }
}
